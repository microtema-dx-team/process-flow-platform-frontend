import VueRouter from 'vue-router'

declare module 'vue/types/vue' {
  interface Vue {
    $notification: (event: any) => void;
    $router: VueRouter;
  }
}
