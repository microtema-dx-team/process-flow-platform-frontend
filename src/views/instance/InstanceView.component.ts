import Vue from 'vue'
import Component from 'vue-class-component'
import { ProcessInstance } from '@/components'

@Component({
  components: {
    ProcessInstance
  }
})
export default class InstanceViewComponent extends Vue {}
