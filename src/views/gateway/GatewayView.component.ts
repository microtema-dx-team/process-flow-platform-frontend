import Vue from 'vue'
import Component from 'vue-class-component'
import { Gateway } from '@/components'

@Component({
  components: {
    Gateway
  }
})
export default class GatewayViewComponent extends Vue {}
