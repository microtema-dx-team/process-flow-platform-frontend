import Vue from 'vue'
import Component from 'vue-class-component'
import { ProcessReport, ProcessDiagram } from '@/components'

@Component({
  components: {
    ProcessReport,
    ProcessDiagram
  }
})
export default class ReportViewComponent extends Vue {}
