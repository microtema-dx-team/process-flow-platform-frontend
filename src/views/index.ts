import Home from './home/Home.vue'
import Definition from './definition/Definition.vue'
import ReportView from './report/ReportView.vue'
import Callback from './login/Callback.vue'

export { Home, Definition, ReportView, Callback }
