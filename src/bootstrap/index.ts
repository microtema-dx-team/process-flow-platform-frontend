import Vue from 'vue'
/**
 * Register all bootstrap components globally here
 */
import { ProcessStatus } from '@/components'

Vue.component('ProcessStatus', ProcessStatus)
