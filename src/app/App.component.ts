import Vue from 'vue'
import Component from 'vue-class-component'
import { Definition, Home } from '@/views'

@Component({
  components: {
    Home,
    Definition
  }
})
export default class AppComponent extends Vue {
  notifications: Array<any> = [];

  login () {
    Vue.prototype.$auth.signInWithRedirect()
  }

  logout () {
    Vue.prototype.$auth.signOut()
  }

  mounted () {
    Vue.prototype.$notification = this.addNotification.bind(this)
  }

  removeNotification (entry) {
    this.notifications = this.notifications.filter(it => it.id !== entry.id)
  }

  addNotification (entry) {
    entry.id = Math.random().toString(36).substr(2, 9)
    this.notifications.push(entry)
    setTimeout(() => this.removeNotification(entry), 10000)
  }
}
