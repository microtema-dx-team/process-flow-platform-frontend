export const Optional = (it: any | undefined): any => {
  if (it === null || it === undefined) {
    return {}
  }

  return it
}
