import Vue from 'vue'
import Vuex from 'vuex'
import { ProcessDiagramStore, ProcessReportStore } from '@/components'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    ProcessReportStore,
    ProcessDiagramStore
  }
})
