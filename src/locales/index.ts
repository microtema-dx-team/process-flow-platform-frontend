import VueI18n from 'vue-i18n'
import Vue from 'vue'
import humanizeDuration from 'humanize-duration'
import messagesDefault from './messages.json'
import messagesDe from './messages-de.json'

Vue.use(VueI18n)

const i18n = new VueI18n({
  locale: 'en-US',
  fallbackLocale: 'en-US',
  messages: {
    'en-US': messagesDefault,
    de: messagesDe
  },
  dateTimeFormats: {
    'en-US': {
      short: {
        year: 'numeric', month: 'short', day: 'numeric'
      },
      long: {
        year: 'numeric',
        month: 'short',
        day: 'numeric',
        hour: 'numeric',
        minute: 'numeric'
      }
    },
    de: {
      short: {
        year: 'numeric', month: 'short', day: 'numeric'
      },
      long: {
        year: 'numeric',
        month: 'short',
        day: 'numeric',
        hour: 'numeric',
        minute: 'numeric'
      }
    }
  },
  numberFormats: {
    'en-US': {
      currency: { style: 'currency', currency: 'USD' }
    },
    de: {
      currency: { style: 'currency', currency: 'EUR' }
    }
  }
})

export const durationFilter = (key: number): string => {
  if (key < 1000) {
    return 'few milliseconds'
  }
  return humanizeDuration(key, { units: ['d', 'h', 'm', 's'], round: true })
}

export const messageFilter = (key: string, params?: any): string => {
  if (!key) {
    return ''
  }

  return i18n.t(key, params).toString()
}

export const dateFilter = (key: any, ...values: any): string => {
  if (!key) {
    return ''
  }

  if (typeof key === 'string') {
    key = new Date(key as string)
  }

  if (!values || !values.length) {
    values = 'long'
  }

  return i18n.d(key, values).toString()
}

export const messageDirective = (el: any, binding: any) => {
  let value

  if (typeof binding.value === 'string') {
    value = i18n.t(binding.value).toString()
  } else {
    const { key, params } = binding.value

    value = i18n.t(key, params)
  }

  el.setAttribute(binding.arg, value)
}

/**
 * NOTE: i18n filter is a shorthand of Vue.$i18n.t
 */
Vue.filter('translate', messageFilter)

/**
 * NOTE: i18n filter is a shorthand of Vue.$i18n.d
 */
Vue.filter('date', dateFilter)

Vue.filter('duration', durationFilter)

/**
 * NOTE: translate Directive is much faster then message filter,
 *       works only on DOM Elements and not Vue Components.
 */
Vue.directive('translate', messageDirective)

export default i18n
