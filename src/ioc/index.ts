import Vue from 'vue'
import { VueIocPlugin } from '@vue-ioc/core'

Vue.use(VueIocPlugin)
