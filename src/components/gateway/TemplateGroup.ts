export interface TemplateGroup {
  name: string;
  properties: Array<TemplateProperty>;
}

export interface TemplateProperty {
  id: string;
  name: string;
  value?: never;
  label: string;
  groupLabel: string;
  description?: string;
  choices?: Array<ChoiceOption>;
}

export interface ChoiceOption {
  name: string;
  value?: string;
}
