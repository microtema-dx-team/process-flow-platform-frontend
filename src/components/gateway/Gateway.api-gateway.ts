import Axios from 'axios-observable'
import { TemplateGroup } from './TemplateGroup'
import { map } from 'rxjs/operators'
import { Injectable } from '@vue-ioc/core'

@Injectable()
export default class GatewayApiGateway {
  http: Axios = Axios.create({});

  template (process: string) {
    return this.http.get<TemplateGroup>(`/${process}/rest/process/template`)
      .pipe(map(it => it.data))
  }

  startProcess (process: string, data: any) {
    return this.http.post<string>(`/${process}/rest/process/start`, data)
      .pipe(map(it => it.data))
  }
}
