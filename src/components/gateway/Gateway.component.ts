import Vue from 'vue'
import Component from 'vue-class-component'
import GatewayApiGateway from './Gateway.api-gateway'
import { TemplateGroup } from './TemplateGroup'
import { DropdownTemplate, StringTemplate } from '../element-template'
import { Inject, Module } from '@vue-ioc/core'

@Module({
  providers: [GatewayApiGateway]
})
@Component({
  components: {
    StringTemplate,
    DropdownTemplate
  }
})
export default class GatewayComponent extends Vue {
  @Inject()
  apiGateway!: GatewayApiGateway;

  templateGroups: Array<TemplateGroup> = []

  process = { boundedContext: '', definitionKey: '', processName: '' };

  mounted () {
    this.process = { ...this.process, ...this.$route.params }
    this.apiGateway.template(this.process.boundedContext).subscribe((it: TemplateGroup) => {
      it.properties.forEach(p => {
        let group = this.templateGroups.find(g => g.name === p.groupLabel)

        if (!group) {
          group = {
            name: p.groupLabel,
            properties: []
          }

          this.templateGroups.push(group)
        }

        group.properties.push(p)
      })
    })
  }

  componentType (type: string) {
    switch (type) {
      case 'STRING':
        return StringTemplate
      case 'DROPDOWN':
        return DropdownTemplate
    }
  }

  startProcess () {
    const data = { ...this.process }
    this.templateGroups.forEach(it => it.properties.forEach(p => {
      data[p.name] = p.value
    }))
    this.apiGateway.startProcess(this.process.boundedContext, data)
      .subscribe(() => {
        this.$notification({ title: 'Process Start', description: 'Process successful started/queued' })
        this.forwardBack()
      })
  }

  forwardBack () {
    this.$router.push({ name: 'InstanceView' }).then()
  }

  isValidForm () {
    return this.templateGroups.filter(it => it.properties.filter(p => !p.value).length).length === 0
  }
}
