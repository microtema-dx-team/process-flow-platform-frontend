import Vue from 'vue'
import Component from 'vue-class-component'
import { Prop } from 'vue-property-decorator'
import { TemplateProperty } from '@/components/gateway/TemplateGroup'

@Component
export default class StringTemplateComponent extends Vue {
  @Prop()
  data!: TemplateProperty
}
