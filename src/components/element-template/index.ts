import StringTemplate from './string-template/StringTemplate.vue'
import DropdownTemplate from './dropdown-template/DropdownTemplate.vue'

export { StringTemplate, DropdownTemplate }
