export default interface ProcessInstanceModel {
  id: string;
  instanceName: string;
  instanceDescription: number;
  instanceStartTime: string;
  instanceEndTime?: string;
  duration: number;
  instanceStatus: string;
  starterId: string;
  referenceId: string;
  referenceType: string;
}
