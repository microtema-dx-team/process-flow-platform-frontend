import Vue from 'vue'
import Component from 'vue-class-component'
import ProcessInstanceApiGateway from './ProcessInstance.api-gateway'
import ProcessInstanceModel from './ProcessInstance.model'
import { Inject, Module } from '@vue-ioc/core'

@Module({
  providers: [ProcessInstanceApiGateway]
})
@Component
export default class ProcessInstanceComponent extends Vue {
  @Inject()
  apiGateway!: ProcessInstanceApiGateway;

  entries: Array<ProcessInstanceModel> = [];

  mounted () {
    this.apiGateway.query().subscribe((it) => {
      const { content } = it
      this.entries = content
    })
  }

  viewProcess ({ processBusinessKey }: any) {
    this.$router.push({ name: 'ReportView', params: { processBusinessKey } }).then()
  }
}
