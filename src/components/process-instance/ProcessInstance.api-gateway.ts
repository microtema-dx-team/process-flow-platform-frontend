import Axios from 'axios-observable'
import { map } from 'rxjs/operators'
import { Injectable } from '@vue-ioc/core'

@Injectable()
export default class ProcessInstanceApiGateway {
  http: Axios = Axios.create({});

  query () {
    return this.http.get('/api/rest/process').pipe(map((it) => it.data))
  }

  getProcessInstanceByInstanceId (instanceId: string) {
    return this.http.get('/api/rest/process/' + instanceId).pipe(map((it) => it.data))
  }
}
