import Vue from 'vue'
import Component from 'vue-class-component'
import ProcessDefinitionApiGateway from './ProcessDefinition.api-gateway'
import ProcessDefinitionModel from './process-definition.model'
import { Inject, Module } from '@vue-ioc/core'

@Module({
  providers: [ProcessDefinitionApiGateway]
})
@Component
export default class ProcessDefinitionComponent extends Vue {
  @Inject()
  apiGateway!: ProcessDefinitionApiGateway;

  entries: Array<ProcessDefinitionModel> = [];

  mounted () {
    this.apiGateway.query().subscribe((it) => {
      const { content } = it
      this.entries = content
    })
  }

  startProcess ({ boundedContext, definitionKey, displayName }: any) {
    this.$router.push({ name: 'Gateway', params: { boundedContext, definitionKey, processName: displayName } }).then()
  }
}
