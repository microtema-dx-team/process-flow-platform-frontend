import Axios from 'axios-observable'
import { map } from 'rxjs/operators'
import { Injectable } from '@vue-ioc/core'

@Injectable()
export default class ProcessDefinitionApiGateway {
  http: Axios = Axios.create({});

  query () {
    return this.http.get('/api/rest/definition').pipe(map((it) => it.data))
  }
}
