export default interface ProcessDefinitionModel {
  id: string;
  boundedContext: string;
  definitionVersion: number;
  majorVersion: number;
  deployTime: any;
}
