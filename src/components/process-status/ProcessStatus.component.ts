import Vue from 'vue'
import Component from 'vue-class-component'
import { Prop } from 'vue-property-decorator'
import { ProcessStatusModel } from './ProcessStatus.model'

@Component
export default class ProcessStatusComponent extends Vue {
  @Prop()
  data!: ProcessStatusModel
}
