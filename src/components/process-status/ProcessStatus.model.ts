export interface ProcessStatusModel {
  label: string;
  status: string;
}
