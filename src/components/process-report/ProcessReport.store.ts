import ProcessReportModel from '@/components/process-report/ProcessReport.model'
import { StoreOptions, ActionContext } from 'vuex'
import { Optional } from '@/utils'
import * as EVENT from '@/constants/StoreEvent'

interface StoreState {
  selected: ProcessReportModel | null;
  entries: Array<ProcessReportModel>;
}

export default {
  namespaced: true,
  state: {
    selected: null,
    entries: []
  } as StoreState,
  mutations: {
    [EVENT.SELECT]: (state: StoreState, payload: ProcessReportModel) => {
      state.selected = payload
    },
    [EVENT.ENTRIES]: (state: StoreState, payload: Array<ProcessReportModel>) => {
      state.entries = payload
    }
  },
  getters: {
    [EVENT.SELECTED]: (state: StoreState) => state.entries.find((it: ProcessReportModel) => Optional(state.selected).id === it.id),
    [EVENT.ENTRIES]: (state: StoreState) => state.entries
  },
  actions: {
    [EVENT.SELECT]: (context: ActionContext<StoreState, StoreState>, payload: ProcessReportModel) => context.commit(EVENT.SELECT, payload),
    [EVENT.ENTRIES]: (context: ActionContext<StoreState, StoreState>, payload: Array<ProcessReportModel>) => context.commit(EVENT.ENTRIES, payload)
  }
} as StoreOptions<StoreState>
