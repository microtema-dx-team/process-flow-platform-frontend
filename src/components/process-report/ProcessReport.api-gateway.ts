import Axios from 'axios-observable'
import { map } from 'rxjs/operators'
import { Injectable } from '@vue-ioc/core'

@Injectable()
export default class ProcessReportApiGateway {
  http: Axios = Axios.create({});

  query ({ processBusinessKey }: any) {
    return this.http.get('/api/rest/report/?processBusinessKey=' + processBusinessKey).pipe(map((it) => it.data))
  }
}
