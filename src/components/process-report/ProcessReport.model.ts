export default interface ProcessReportModel {
  id: string;
  activityId: string;
  activityName: string;
  processStatus: string;
  description: string;
  reportStartTime: string;
  reportEndTime?: string;
  duration: number;
}
