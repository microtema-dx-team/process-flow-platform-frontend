import Vue from 'vue'
import Component from 'vue-class-component'
import { namespace } from 'vuex-class'
import { Inject, Module } from '@vue-ioc/core'

import { Optional } from '@/utils'
import ProcessReportApiGateway from './ProcessReport.api-gateway'
import ProcessReportModel from './ProcessReport.model'
import * as EVENT from '@/constants/StoreEvent'

const ModuleStore = namespace('ProcessReportStore')

@Module({
  providers: [ProcessReportApiGateway]
})
@Component
export default class ProcessReportComponent extends Vue {
  @Inject()
  apiGateway!: ProcessReportApiGateway;

  @ModuleStore.Getter(EVENT.ENTRIES)
  entries?: Array<ProcessReportModel>;

  @ModuleStore.Getter(EVENT.SELECTED)
  selected?: ProcessReportModel;

  @ModuleStore.Action(EVENT.ENTRIES)
  updateStore!: (payload: Array<ProcessReportModel>) => Promise<void>;

  @ModuleStore.Action(EVENT.SELECT)
  selectReport!: (payload: ProcessReportModel) => Promise<void>;

  mounted () {
    this.apiGateway.query(this.$route.params).subscribe(this.updateStore.bind(this))
  }

  isSelected (entry: ProcessReportModel) {
    return Optional(this.selected).activityId === entry.activityId
  }
}
