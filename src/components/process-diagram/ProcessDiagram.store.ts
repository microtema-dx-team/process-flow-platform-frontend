import { StoreOptions } from 'vuex'

interface StoreState {
  selected: any;
}

export default {
  namespaced: true,
  name: 'ProcessDiagramStore',
  state: {
    selected: null
  } as StoreState,
  mutations: {
    selected: (state: any, payload: any) => { state.selected = payload }
  },
  getters: {
    selected: (state: any) => state.selected
  },
  actions: {
    selected: (context: any, payload: any) => context.commit('selected', payload)
  }
} as StoreOptions<StoreState>
