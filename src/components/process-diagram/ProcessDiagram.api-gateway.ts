import Axios from 'axios-observable'
import { map, mergeMap } from 'rxjs/operators'
import { Injectable } from '@vue-ioc/core'

@Injectable()
export default class ProcessDiagramApiGateway {
  http: Axios = Axios.create({});

  query () {
    return this.http.get('/api/rest/process').pipe(map((it) => it.data))
  }

  getProcessDiagram (processBusinessKey: string) {
    return this.http.get('/api/rest/process/' + processBusinessKey)
      .pipe(mergeMap((it: any) => this.http.get('/api/rest/definition/key/' + it.data.definitionKey)),
        map((it: any) => it.data.diagram))
  }
}
