import Vue from 'vue'
import Component from 'vue-class-component'
import BpmnJS from 'bpmn-js'
import { namespace } from 'vuex-class'
import { Watch } from 'vue-property-decorator'
import { Inject, Module } from '@vue-ioc/core'

import * as EVENT from '@/constants/StoreEvent'

import ProcessReportModel from '@/components/process-report/ProcessReport.model'
import ProcessDiagramApiGateway from './ProcessDiagram.api-gateway'

const ModuleStore = namespace('ProcessReportStore')

@Module({
  providers: [ProcessDiagramApiGateway]
})
@Component
export default class ProcessDiagramComponent extends Vue {
  viewer: BpmnJS;

  canvas: any;

  eventBus: any;

  @Inject()
  apiGateway!: ProcessDiagramApiGateway;

  @ModuleStore.Getter(EVENT.ENTRIES)
  reports!: Array<ProcessReportModel>;

  @ModuleStore.Getter(EVENT.SELECTED)
  selected!: ProcessReportModel;

  @ModuleStore.Action(EVENT.SELECT)
  selectElement!: (payload: any) => Promise<void>;

  prevSelected?: ProcessReportModel;

  @Watch('selected')
  onSelectedElement (e: any) {
    if (this.prevSelected) {
      this.canvas.removeMarker(this.prevSelected.activityId, 'selected')
    }
    if (e.activityId) {
      this.canvas.addMarker(e.activityId, 'selected')
    }
    this.prevSelected = e
  }

  mounted () {
    const { processBusinessKey } = this.$route.params
    this.viewer = new BpmnJS({
      container: '.diagram-container'
    })
    this.apiGateway.getProcessDiagram(processBusinessKey).subscribe(this.loadDiagram.bind(this))
  }

  loadDiagram (diagram: string) {
    this.viewer.importXML(diagram).then(() => {
      this.canvas = this.viewer.get('canvas')
      this.eventBus = this.viewer.get('eventBus')
      this.eventBus.on('selection.changed', this.onSelectionChanged.bind(this))
      this.canvas.zoom('fit-viewport', true)
      this.addMarkers()
    })
  }

  unmounted () {
    this.viewer.detach()
  }

  addMarkers () {
    this.reports.forEach(it => this.canvas.addMarker(it.activityId, it.processStatus))
  }

  onSelectionChanged (e: any) {
    const newSelection = e.newSelection[0]
    if (!newSelection) {
      return
    }
    const selected = this.reports.find(it => it.activityId === newSelection.id)
    this.onSelectedElement(selected)
    this.selectElement(selected).then()
  }
}
