module.exports = {
  runtimeCompiler: true,
  devServer: {
    proxy: {
      '^/api': {
        target: 'http://localhost:30081',
        changeOrigin: true,
        logLevel: 'debug'
      },
      '^/domain': {
        target: 'http://localhost:30092',
        changeOrigin: true,
        logLevel: 'debug',
        pathRewrite: {
          '^/domain': '/domain-service'
        }
      }
    }
  }
}
