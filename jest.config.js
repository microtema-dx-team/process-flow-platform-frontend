module.exports = {
  preset: '@vue/cli-plugin-unit-jest/presets/typescript',
  testMatch: [
    '**/src/**/*.spec.(js|jsx|ts|tsx)'
  ],
  collectCoverage: true,
  collectCoverageFrom: ['**/*.{ts,vue}', '!**/node_modules/**', '!**/*.d.ts', '!**/*.vue']
}
